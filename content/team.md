---
title: "Team"
layout: "about"
draft: false

# our team
our_team:
  enable: true
  subtitle: "<br><br>The people behind Unity"
  title: "Team"
  team:
  - name: "Rudra Saraswat (`@RudraSaraswat1` on Twitter)"
    image: "images/about/team/rs2009.gif"
    designation: "**Developer, Ubuntu Member and project lead of Ubuntu Unity**"
  - name: "Khurshid Alam"
    image: "images/about/team/khurshid-alam.jpg"
    designation: "**Maintaining Unity7 since 2017**"

---
