---
date: "2022-04-21"
title: "Unity 7.6 released for testing"
image: "images/blog/unity7.6/redesigned_dash.png"
author_info: 
  name: "Rudra Saraswat"
  image: "images/about/team/rs2009.gif"
draft: false
---

Unity 7.6 will be the first major release of Unity in 6 years (the last release was in May 2016). We have restarted the active development of Unity7 and will be releasing new versions with more features regularly. You can test it by running the following commands on Ubuntu Unity 22.04 (we'll be rolling it out to existing users of Ubuntu Unity 22.04 too, if you don't add the testing PPA):

```sh
sudo wget https://repo.unityx.org/unityx.key
sudo apt-key add unityx.key
echo 'deb https://repo.unityx.org/main testing main' | sudo tee /etc/apt/sources.list.d/unity-x.list
sudo apt-get update && sudo apt-get install -y unity
```

**Here are the changes I've made in Unity 7.6:**

* The dash (app launcher) and HUD have been redesigned for a modern and slick look.
* Fixed broken app info and ratings in dash preview.
* Fixed the 'Empty Trash' button in the dock (it now uses Nemo instead of Nautilus).
* Migrated the complete Unity7 shell source code to GitLab.
* The design is much flatter but retains the system-wide blur.
* The dock's menus and tooltips have been given a more modern look.
* The RAM usage in Unity7 is slightly lower now, while the RAM usage has been reduced substantially to about 700-800 MBs in Ubuntu Unity 22.04.
* Fixed the standalone testing Unity7 launcher (this will help Unity7 contributors).

**Here are the commands you need to run if you want to compile Unity7 on Ubuntu Unity 22.04 and generate the DEB files:**

```sh
sudo apt-get install -y build-essential git cmake
git clone https://gitlab.com/ubuntu-unity/unity/unity
cd unity && sudo apt build-dep .
debuild -b --no-sign
nemo .. & disown
```

**Here are some screenshots of Unity 7.6:**

<img src="/images/blog_contents/unity7.6/neofetch.png"></img>
<img src="/images/blog_contents/unity7.6/new_app_preview.png" width="100%" height="100%"></img>
<img src="/images/blog_contents/unity7.6/redesigned_dash.png" width="100%" height="100%"></img>
<img src="/images/blog_contents/unity7.6/updated_app_menu.jpg" width="50%" height="50%"></img>