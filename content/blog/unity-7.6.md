---
date: "2022-06-30"
title: "Announcing the stable release of Unity 7.6"
image: "images/blog/unity7.6/final_release.png"
author_info: 
  name: "Rudra Saraswat"
  image: "images/about/team/rs2009.gif"
draft: false
---

Unity 7.6 will be the first major release of Unity in 6 years (the last release was in May 2016). We have restarted the active development of Unity7 and will be releasing new versions with more features regularly. An update has been released for Ubuntu Unity 22.04 users, so run `sudo apt update && sudo apt upgrade` to upgrade to Unity 7.6, or you can wait for the software updater to check for updates.

**Here are the changes I've made in Unity 7.6:**

* The dash (app launcher) and HUD have been redesigned for a modern and slick look.
* Added support for accent colors to Unity and unity-control-center, and updated theme list in unity-control-center.
* Fixed broken app info and ratings in dash preview.
* The info panel has been updated in unity-control-center.
* Improved dash rounded corners.
* Fixed the 'Empty Trash' button in the dock (it now uses Nemo instead of Nautilus).
* Migrated the complete Unity7 shell source code to GitLab and got it to compile on 22.04.
* The design is much flatter but retains the system-wide blur.
* The dock's menus and tooltips have been given a more modern look.
* The low graphics mode works much better now and the dash is faster than ever.
* The RAM usage in Unity7 is slightly lower now, while the RAM usage has been reduced substantially to about 700-800 MBs in Ubuntu Unity 22.04.
* Fixed the standalone testing Unity7 launcher (this will help Unity7 contributors).
* The buggy tests have been disabled and the build time is much shorter (this will help Unity7 contributors).

**Here are some screenshots of Unity 7.6:**

<img src="/images/blog_contents/unity7.6/neofetch.png"></img>
<img src="/images/blog_contents/unity7.6/final_release.png" width="100%" height="100%"></img>
<img src="/images/blog_contents/unity7.6/updated_app_menu.jpg" width="50%" height="50%"></img>