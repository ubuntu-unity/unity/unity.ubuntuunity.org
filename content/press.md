---
title: "Press"
layout: "about"
draft: false

# what_we_do
what_we_do:
  enable: true
  title: "<br>Press Coverage"
  block:

  - title: "TechRepublic"
    content: "\"Ubuntu Unity is not the desktop pariah you once thought it was. This desktop environment has evolved into a beautiful, efficient interface that does not deserve the scorn and derision heaped upon it by so many.\" **- Jack Wallen**

    <br><br>[**Read more >>**](https://www.techrepublic.com/article/ubuntu-unity-brings-back-one-of-the-most-efficient-desktops-ever-created/)"

  - title: "9to5Linux"
    content: "\"The Ubuntu Unity team released today Ubuntu Unity 21.10 as the latest version of their unofficial Ubuntu flavor featuring the good old Unity desktop environment.\" **- Marius Nestor**

    <br><br>[**Read more >>**](https://9to5linux.com/ubuntu-unity-21-10-released-to-keep-the-unity-desktop-alive-in-2021)"

  - title: "Wikipedia"
    content: "\"Yes, this was the old Unity I used to know and love, but somehow it felt fresher. As I worked to regain muscle memory over the key-bindings (GNOME really can take over the way you control your system XD), the experience was smooth, graceful, and fun in a way that is unique to the Unity experience.\"

    <br><br>[**Read more >>**](https://medium.com/linux-plus-plus/linux-may-17-2020-aff97aed3a05)"

---
