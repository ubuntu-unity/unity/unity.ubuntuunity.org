---
# banner
banner:
  title: "Unity's back!"
  button: "Latest release"
  button_link: "/blog/unity-7.6/"
  image: "images/laptop-ubuntuunity.png"


# brands
brands_carousel:
  enable: false
  brand_images:
  - " "


# features
features:
  enable: true
  title: "Features"
  description: "Here are the latest features in Unity 7.6."
  features_blocks:
  - icon: "las la-brush"
    title: "A redesigned look"
    content: "This is the first release of Unity in 6 years (the last release was back in May 2016). Unity 7.6 has a redesigned look, with elements like the dash and HUD having a much flatter UI, while still retaining the blur."
  - icon: "las la-gift"
    title: "Unity7 Goodies"
    content: "Unity 7.6 continues to have the HUD and Global Menu, so that you can continue using the features of Unity7 which you had used on the previous versions of Ubuntu before 17.10 Artful Aardvark."
  - icon: "las la-bug"
    title: "Bug fixes"
    content: "Most of the bugs which were present in Unity 7.5 on new versions of Ubuntu have been fixed, like the trash icon not working when Nautilus is not installed or the app previews in the dash being broken."


# intro_video
intro_video:   
  enable: true
  subtitle: "Linux For Everyone"
  title: "A review of Ubuntu Unity 20.04"
  description: "A video about the first release of Ubuntu Unity (released in May 2020)."
  video_url: "https://www.youtube.com/embed/VAbD99Zk2Jc"
  video_thumbnail: "images/l4e.jpg"

# testimonials
testimonials:   
  enable: true
  title: "<br><br>Testimonials"
  image_left: "none"
  image_right: "none"
  
  testimonials_quotes:

  - quote: "Ubuntu Unity is not the desktop pariah you once thought it was. This desktop environment has evolved into a beautiful, efficient interface that does not deserve the scorn and derision heaped upon it by so many."
    name: "Jack Wallen"
    designation: "TechRepublic"
    image: "none"

  - quote: "I took Ubuntu Unity Remix 20.04 for a spin and it brought back good old memories for me. The spin looks great and works like a charm."
    name: "Marius Nestor"
    designation: "9to5Linux"
    image: "none"

  - quote: "Yes, this was the old Unity I used to know and love, but somehow it felt fresher. As I worked to regain muscle memory over the key-bindings (GNOME really can take over the way you control your system XD), the experience was smooth, graceful, and fun in a way that is unique to the Unity experience."
    name: "Eric Londo"
    designation: "Linux++, Destination Linux Network"
    image: "none"

---
